**Anggota kelompok : **
1. Batara Yehezkiel Situmorang - 2006464000
2. Shabrina Salsabila Kurniawan - 2006464745
3. Aulia Nur Fadhilah - 2006473491
4. Nadinda Kalila - 2006485056
5. Richie Christiansen Senlia - 2006473895
6. Muhammad Devin Pratista - 2006485781
7. Venska Aurell Dominica - 2006464783

**Link menuju download APK **
https://ristek.link/TugasAkhirB01

**Cerita daftar modul yang akan diimplementasikan dan bagaimana integrasinya dengan Web Service atas halaman Web sebelumnya : **
<div align="justify">

1. Landing page (introduction, sign up, dan sign in) : saat pertama kali user mengunjungi link website, user akan melihat pengenalan website tersebut serta pilihan sign-in dan sign-up. Modul ini akan mengirim data ke web service django melalui method post untuk kemudian dilakukan autentikasi.
2. To Do (list dan deadline) : fitur yang dapat digunakan untuk menambahkan list aktivitas beserta tanggal deadlinenya. Modul ini akan mengirim data berupa task dan deadline ke database django, lalu untuk menampilkan to do list, modul ini akan mengakses web service django untuk mendapatkan data daftar to do list milik user. 
3. Take a Note (bisa untuk publik dan private) : fitur untuk membuat dan menyimpan catatan pada website. Pengguna dapat memanfaatkan fitur ini untuk menulis jurnal, diary, catatan pelajaran, dll. Modul ini akan mengirim data note ke database django. Lalu,  modul ini akan mengambil data note atau catatan yang dimiliki user untuk menampilkannya pada halaman Take a Note.
4. Profile : page yang menunjukkan informasi terkait pada profil pengguna. Fitur ini dapat digunakan user untuk melihat dan mengubah username, email yang didaftarkan saat sign up, dan nomor telepon. Modul ini akan mengambil data username, email, dan nomor telepon suatu user pada web service django. Untuk me-reuse sebuah URL views yang akan dipanggil oleh profile, terdapat suatu method pada views.py yang bertujuan untuk mengirim data dari model UserProfile (HttpResponse) pada Django dalam format XML yang nanti akan di fetch dari sisi Flutter. 
5. Home, search profile, navbar : bagian home berisi reminder aktivitas-aktivitas yang dimiliki user, data ini diperoleh dengan mengambil data kumpulan list tugas dari web service django. Terdapat juga fitur search profile yang dapat digunakan user untuk mencari profil dari user lain. Data untuk memperoleh username ini diambil dari database setelah user melakukan sign up dari web service django. Selain itu, terdapat navigation bar yang akan mengarahkan user ke fitur-fitur lain. 
6. Public Blogs: Fitur ini dapat digunakan oleh user untuk menulis sebuah artikel/blog yang kemudian dapat dipublikasikan pada website tersebut. Terdapat beberapa halaman pada fitur ini meliputi halaman publikasi, halaman utama public blogs, dan halaman blog dari masing-masing blog. Pada halaman publikasi, akan tersedia form berupa text editor bagi user untuk menuliskan artikel yang ingin dipublikasikan dan halaman ini akan mengambil username penulis artikel tersebut dari database web service django. Pada halaman utama public blogs, flutter akan mengambil data setiap blog yang pernah dibuat dari database web django, beserta juga pada masing-masing halaman blog.
7. Message ke user lain : fitur untuk mengirim pesan kepada user lain. Pada modul ini akan diambil data message dan contact yang tersimpan pada database django. module ini juga memiliki fitur kirim pesan yang akan mengirim pesan baru kepada database django.

