import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:http/http.dart' as http;
import 'package:tugasakhirb01/global.dart';
import 'package:tugasakhirb01/home/SideBar.dart';
import 'dart:convert';

import 'package:tugasakhirb01/global.dart' as globals;

import 'card_note.dart';
// import 'package:lab_7/screen/card_note.dart';

class NoteForm extends StatelessWidget {
  const NoteForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const appTitle = 'Take A Note App';

    return MaterialApp(
      title: appTitle,
      home: Scaffold(
        body: MyCustomForm(),
      ),
    );
  }
}

// Create a Form widget.
class MyCustomForm extends StatefulWidget {
  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

class MyCustomFormState extends State<MyCustomForm> {
  final _formKey = GlobalKey<FormState>();
  String judul = "";
  String isiNote = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideBar(),
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children:<Widget>[
            Text("좋은날", style:GoogleFonts.poppins(fontSize: 20, color: Color(0xFF7862AE)),),
            Text(" || Note",style: GoogleFonts.poppins(fontSize: 20),),
          ],
        ),
        backgroundColor: Color(0xFF302C2C),
        // Text("Joeurnnal 좋은날",
        //       style: GoogleFonts.poppins(),
        // // ),
        // backgroundColor: Colors.grey[900],
      ),
      body: 
      Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/backgpag.png'),
              fit: BoxFit.cover,
            ),
          ),

          child:
      Padding(
        padding: EdgeInsets.all(25.0),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(15)),
            color: Color.fromRGBO(141, 129, 204, 1)
          ),
          padding: EdgeInsets.only(left: 25, right: 25),
          child: 
        Center(
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Take A Note',
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white, fontFamily: 'Poppins'),
                ),
                Padding(
                  padding: EdgeInsets.only(
                    top: 20.0,
                  ),
                ),
                TextFormField(
                  style: GoogleFonts.poppins(
                        color: Colors.black,
                  ),
                  decoration: InputDecoration(
                    filled: true,
                      fillColor: Colors.white,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(
                          color: Color.fromRGBO(141, 129, 204, 1),
                        ),
                      ),
                      labelText: "Title"),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please add your title note';
                    }
                    judul = value;
                    value = "";
                    return null;
                  },
                ),
                Padding(
                  padding: EdgeInsets.only(
                    top: 20.0,
                  ),
                ),
                TextFormField(
                  maxLines: 7,
                  style: GoogleFonts.poppins(
                        color: Colors.black,
                        ),
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.white,
                    labelText: "Notes",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(
                        color: Color.fromRGBO(141, 129, 204, 1),
                      ),
                    ),
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please add your note';
                    }
                    isiNote = value;
                    value = "";
                    return null;
                  },
                ),
                Padding(
                  padding: EdgeInsets.only(
                    top: 20.0,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      ElevatedButton(
                        // shape: RoundedRectangleBorder(
                        // borderRadius: BorderRadius.circular(10.0)),
                        // color: Color.fromRGBO(141, 129, 204, 1),
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            sendData(judul, isiNote, globals.GlobalData.user);
                            // print("Title: " + judul);
                            // print("Note:\n" + isiNote);

                            showDialog(
                                context: context,
                                builder: (context) {
                                  return AlertDialog(
                                    content: Text(
                                        "Succesfully save your note!",
                                        style: GoogleFonts.poppins(),
                                        ),
                                    actions: <Widget>[
                                      TextButton(
                                        onPressed: () {
                                          Navigator.of(context).push(
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      NoteForm()));
                                        },
                                        child: Text('OK'),
                                      ),
                                    ],
                                  );
                                });
                          }
                        },

                        
                        child: Text("Save",
                              style: GoogleFonts.poppins(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                            )
                            ),
                        style: TextButton.styleFrom(
                          backgroundColor: Colors.white,
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0),), 
                          padding: EdgeInsets.only(top: 20, bottom: 20,left: 25, right: 25),
                          )
                        // Text("Save", style: TextStyle(color: Colors.white)),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 40.0,),
                      ),
                      ElevatedButton(
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => CardNote2()));
                        },
                        child: Text("Cancel",
                            style: GoogleFonts.poppins(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                            )
                            ),
                        style: TextButton.styleFrom(
                          backgroundColor: Colors.white,
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0),), 
                          padding: EdgeInsets.only(top: 20, bottom: 20,left: 20, right: 20),
                          )
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        ),
      ),
      ),
    );
  }
}

Future<http.Response> sendData(var _judul, var _isi, var _akun) async {
  return http.post(
    Uri.parse('http://joeurnnal.herokuapp.com/take-a-note/send-data'),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, String>{
      'judul': _judul,
      'isi': _isi,
      'akun': _akun,
    }),
  );
}
