import 'package:flutter/material.dart';
import 'package:cool_alert/cool_alert.dart';
import './todo.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:tugasakhirb01/global.dart' as globals;
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(MaterialApp(
    title: "Joeurnnal",
    theme: ThemeData(
      primarySwatch: Colors.deepPurple,
    ),
    home: BelajarForm(),
  ));
}

Future<http.Response> sendData(var _task,var _deadline, var _akun) async{
  return http.post(Uri.parse('http://joeurnnal.herokuapp.com/todo/data'),
  headers: <String,String>{
    'Content-Type' : 'application/json; charset=UTF-8',
  },
  body: jsonEncode(<String,String>{
    'task': _task,
    'deadline': _deadline,
    'akun' : _akun,
  }),
  );
}

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();
  DateTime date = DateTime.now();
  var _task = "";
  var _deadline = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              "좋은날",
              style:
                  GoogleFonts.poppins(fontSize: 20, color: Color(0xFF7862AE)),
            ),
            Text(
              " || To Do List",
              style: GoogleFonts.poppins(fontSize: 20),
            ),
          ],
        ),
        backgroundColor: Color(0xFF302C2C),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    style: GoogleFonts.poppins(
                          color: Colors.black,
                    ),
                    decoration: new InputDecoration(
                      labelText: "Task",
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {

                      if (value!.isEmpty) {
                        return 'Task harus diisi';
                      }
                      print(value);
                      _task = value;
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    style: GoogleFonts.poppins(
                          color: Colors.black,
                    ),
                    decoration: new InputDecoration(
                      labelText: "Deadline",
                      hintText: "YYYY-MM-DD",
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                     validator: (value) {
                      if (value!.isEmpty) {
                        return 'Deadline harus diisi';
                      }
                      print(value);
                      _deadline = value;
                      return null;
                    },
                  ),
                ),
                RaisedButton(onPressed: (){
                 
                  CoolAlert.show(
                    context: context,
                    type: CoolAlertType.success,
                    text: "New task added successfully. Move to the list page",
                  );
                  if (_formKey.currentState!.validate()) {
                      (sendData(_task,_deadline, globals.GlobalData.user));
                  }
                }, 
                elevation: 10.0,
                color: Color(0xFF7862AE),
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
                child: Text('Add', style: TextStyle(color: Colors.white,  fontFamily: 'Poppins',),),           
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}


