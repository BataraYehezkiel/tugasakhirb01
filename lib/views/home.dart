import 'package:flutter/material.dart';
import 'package:tugasakhirb01/views/blogpage.dart';
import 'package:tugasakhirb01/views/createblog.dart';
import 'package:http/http.dart' as http;
import 'package:tugasakhirb01/entities/blogs.dart';
import 'package:tugasakhirb01/home/SideBar.dart';
import 'dart:convert';
import 'package:google_fonts/google_fonts.dart';

Future<List<dynamic>> fetchBlog() async {
  final response =
      await http.get(Uri.parse('https://joeurnnal.herokuapp.com/Group/data/'));

  List<dynamic> result = [];
  if (response.statusCode == 200) {
    result = jsonDecode(utf8.decode(response.bodyBytes))
        .map((e) => BlogsModel.fromJson(e))
        .toList();
  } else {
    throw Exception('Failed to load blogs');
  }
  return result;
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  late Future<List<dynamic>> futureBlog;

  @override
  void initState() {
    super.initState();
    futureBlog = fetchBlog();
  }

  Widget buildListTile(String title, IconData icon, Function tapHandler) {
    return ListTile(
        leading: Icon(
          icon,
          size: 22,
        ),
        title: Text(
          title,
          style: GoogleFonts.poppins(fontSize: 20),
        ),
        onTap: () => tapHandler);
  }

  @override
  Widget build(BuildContext context) {
    final Widget blogsTile = FutureBuilder<List<dynamic>>(
        future: futureBlog,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
                itemCount: snapshot.data?.length,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () => Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) =>
                            BlogPage(dataBlog: snapshot.data?[index]))),
                    child: Card(
                      elevation: 10.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0)),
                      color: Color(0xFF7862AE),
                      child: Padding(
                        padding: const EdgeInsets.only(
                            top: 25.0, bottom: 25.0, left: 16.0, right: 16.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              snapshot.data?[index].fields.title,
                              style: GoogleFonts.poppins(
                                  fontSize: 22,
                                  fontWeight: FontWeight.normal,
                                  color: Colors.white),
                            ),
                            SizedBox(
                              height: 2,
                            ),
                            Text(
                              "Published by ${snapshot.data?[index].fields.author}",
                              style: GoogleFonts.poppins(
                                  fontSize: 16, color: Color(0xff221833)),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                });
          }
          return const Center(
            child: CircularProgressIndicator(color: Color(0xFF7862AE)),
          );
        });

    return Scaffold(
        drawer: SideBar(),
        appBar: AppBar(
          centerTitle: true,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                "좋은날",
                style:
                    GoogleFonts.poppins(fontSize: 20, color: Color(0xFF7862AE)),
              ),
              Text(
                " || Blogs",
                style: GoogleFonts.poppins(fontSize: 20),
              ),
            ],
          ),
          backgroundColor: Color(0xFF302C2C),
        ),
        // drawer: Drawer(
        //   child: Column(
        //     children: <Widget>[
        //       Container(
        //         height: 120,
        //         width: double.infinity,
        //         padding: EdgeInsets.all(18),
        //         alignment: Alignment.center,
        //         color: Color(0xFF7862AE),
        //         child: Text(
        //           'Joeurnnal 좋은날',
        //           style: TextStyle(
        //             fontSize: 30,
        //             color: Colors.white,
        //           ),
        //         ),
        //       ),
        //       SizedBox(
        //         height: 20,
        //       ),
        //       buildListTile('Home', Icons.home, () {
        //         Navigator.of(context).pushReplacementNamed('/');
        //       }),
        //       buildListTile('Profile', Icons.person, () {
        //         Navigator.of(context).pushReplacementNamed('/');
        //       }),
        //       buildListTile('Notes', Icons.notes, () {
        //         Navigator.of(context).pushReplacementNamed('/');
        //       }),
        //       buildListTile('To Do', Icons.add_task, () {
        //         Navigator.of(context).pushReplacementNamed('/');
        //       }),
        //       buildListTile('Public Blogs', Icons.text_snippet, () {
        //         Navigator.of(context).pushReplacementNamed('/');
        //       }),
        //       buildListTile('Message', Icons.aod, () {
        //         Navigator.of(context).pushReplacementNamed('/');
        //       }),
        //     ],
        //   ),
        // ),

        //WIEJWIEJWJELK
        // body: Container(
        //     decoration: BoxDecoration(
        //       image: DecorationImage(
        //         image: AssetImage('assets/images/backgpag.png'),
        //         fit: BoxFit.cover,
        //       ),
        //     ),
        //     child: SizedBox(
        //       children: [
        //         Text(
        //           "Read our featured blogs",
        //           style: GoogleFonts.poppins(fontSize: 30),
        //         ),
        //         blogsTile,
        //       ],
        //     )),

        body: Column(
          children: <Widget>[
            SizedBox(
              height: 20,
            ),
            Text(
              "Read our featured blogs",
              style: GoogleFonts.poppins(fontSize: 30),
            ),
            SizedBox(
              height: 3,
            ),
            Text(
              "All written by our beloved users",
              style: GoogleFonts.poppins(fontSize: 15),
            ),
            SizedBox(
              height: 18,
            ),
            Expanded(
              child: Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/images/backgpag.png'),
                      fit: BoxFit.cover,
                    ),
                  ),
                  height: 200.0,
                  child: blogsTile),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
        )
        // floatingActionButton: Container(
        //   padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
        //   child: Row(
        //     mainAxisAlignment: MainAxisAlignment.end,
        //     children: <Widget>[
        //       FloatingActionButton(
        //         backgroundColor: Color(0xFF7862AE),
        //         child: Icon(
        //           Icons.add,
        //           color: Colors.white,
        //         ),
        //         onPressed: () {
        //           Navigator.push(context,
        //               MaterialPageRoute(builder: (context) => CreateBlog()));
        //         },
        //       )
        //     ],
        //   ),
        // ),
        );
  }
}

// @override
// void initState() {
//   fetchData().then((value) {
//     extractedData = value;
//   });
// }

// class BlogsThumbnail extends StatelessWidget {
//   String imgUrl, author, title, content;
//   BlogsThumbnail(
//       {@required this.imgUrl,
//       @required this.author,
//       @required this.title,
//       @required this.content});

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       child: Stack(
//         children: <Widget>[
//           ClipRRect(
//               borderRadius: BorderRadius.circular(5),
//               child: Image.network(imgUrl)),
//           Container(
//             height: 150,
//             decoration: BoxDecoration(
//                 color: Colors.black26.withOpacity(0.5),
//                 borderRadius: BorderRadius.circular(5)),
//           ),
//           Container(
//             child: Column(
//               children: <Widget>[Text(title), Text(author)],
//             ),
//           )
//         ],
//       ),
//     );
//   }
// }

// Future<void> fetchData() async {
//   //const url = 'http://joeurnnal.herokuapp.com/Group/data/';
//   const url = 'http://127.0.0.1:8000/Group/data/';
//   try {
//     final response = await http.get(Uri.parse(url));
//     print(response.body);
//     Map<String, dynamic> extractedData = jsonDecode(response.body);
//     extractedData.forEach((key, val) {
//       print(val);
//     });
//   } catch (error) {
//     print(error);
//   }
// }
