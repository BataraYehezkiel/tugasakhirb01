import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:google_fonts/google_fonts.dart';

class CreateBlog extends StatefulWidget {
  @override
  _CreateBlogState createState() => _CreateBlogState();
}

class _CreateBlogState extends State<CreateBlog> {
  final _formKey = GlobalKey<FormState>();
  var author = "";
  var title = "";
  var content = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "좋은날",
                style:
                    GoogleFonts.poppins(fontSize: 20, color: Color(0xFF7862AE)),
              ),
              Text(
                " || Blogs",
                style: GoogleFonts.poppins(fontSize: 20),
              ),
            ],
          ),
          backgroundColor: Color(0xFF302C2C),
          elevation: 0.0,
          // actions: <Widget>[
          //   Container(
          //       padding: EdgeInsets.symmetric(horizontal: 18),
          //       child: Icon(Icons.file_upload))
          // ],
        ),
        body: Container(
            child: Column(
          children: <Widget>[
            SizedBox(height: 10),
            // Container(
            //     margin: EdgeInsets.symmetric(horizontal: 18),
            //     height: 150,
            //     decoration: BoxDecoration(
            //         color: Colors.white,
            //         borderRadius: BorderRadius.circular(4)),
            //     width: MediaQuery.of(context).size.width,
            //     child: Icon(
            //       Icons.add_a_photo,
            //       color: Colors.black38,
            //     )),
            SizedBox(
              height: 8,
            ),
            Form(
              key: _formKey,
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 18),
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      decoration: InputDecoration(hintText: "Author"),
                      onChanged: (val) {
                        author = val;
                      },
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Author must be filled';
                        }
                        print(value);
                        //author = value;
                        return null;
                      },
                    ),
                    TextFormField(
                      decoration: InputDecoration(hintText: "Title"),
                      onChanged: (val) {
                        title = val;
                      },
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Title must be filled';
                        }
                        print(value);
                        //title = value;
                        return null;
                      },
                    ),
                    TextFormField(
                      decoration:
                          InputDecoration(hintText: "Write your content here"),
                      onChanged: (val) {
                        content = val;
                      },
                      maxLines: 17,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'You must write a content';
                        }
                        print(value);
                        //content = value;
                        return null;
                      },
                    ),
                    SizedBox(height: 25),
                    ElevatedButton(
                      child: Text("Submit"),
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          print(sendData(author, title, context));
                        }
                      },
                      style: ElevatedButton.styleFrom(
                          primary: Color(0xFF7862AE),
                          padding: EdgeInsets.symmetric(
                              horizontal: 40, vertical: 20),
                          textStyle: TextStyle(fontSize: 14)),
                    ),
                  ],
                ),
              ),
            )
          ],
        )));
  }
}

Future<http.Response> sendData(var __author, var __title, var __content) async {
  return http.post(
    Uri.parse('http://127.0.0.1:8000/Group/get/'),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, dynamic>{
      'author': __author,
      'title': __title,
      'content': __content
    }),
  );
}
