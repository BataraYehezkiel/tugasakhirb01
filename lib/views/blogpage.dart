import 'package:flutter/material.dart';
import 'package:tugasakhirb01/views/createblog.dart';
import 'package:http/http.dart' as http;
import 'package:tugasakhirb01/entities/blogs.dart';
import 'dart:convert';
import 'package:flutter_html/flutter_html.dart';
import 'package:html/dom.dart' as dom;
import 'package:google_fonts/google_fonts.dart';

class BlogPage extends StatelessWidget {
  final BlogsModel dataBlog;

  BlogPage({
    Key? key,
    required this.dataBlog,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var title = this.dataBlog.fields!.title;
    var author = this.dataBlog.fields!.author;
    var content = this.dataBlog.fields!.content;

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              "좋은날",
              style:
                  GoogleFonts.poppins(fontSize: 20, color: Color(0xFF7862AE)),
            ),
            Text(
              " || Blogs",
              style: GoogleFonts.poppins(fontSize: 20),
            ),
          ],
        ),
        backgroundColor: Color(0xFF302C2C),
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/backgpag.png'),
            fit: BoxFit.fitHeight,
          ),
        ),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: const EdgeInsets.only(
                      top: 20.0, bottom: 0.0, left: 16.0, right: 16.0),
                  child: Text(
                    "${title}",
                    style: GoogleFonts.poppins(
                        fontSize: 40, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: const EdgeInsets.only(
                      top: 0.0, bottom: 10.0, left: 16.0, right: 16.0),
                  child: Text(
                    "Author: ${author}",
                    style: GoogleFonts.poppins(
                        fontSize: 16, fontWeight: FontWeight.normal),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Center(
                  child: Card(
                    elevation: 20.0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(14.0)),
                    color: Color(0xffd2cce0),
                    // child: Text(
                    //   "${content}",
                    // ),
                    child: Html(
                      data: "${content}",
                      padding: EdgeInsets.all(20.0),
                      onLinkTap: (url) {
                        print("Opening $url...");
                      },
                      //backgroundColor: Colors.white,
                      // customRender: (node, children) {
                      //   if (node is dom.Element) {
                      //     switch (node.localName) {
                      //       case "custom_tag": // using this, you can handle custom tags in your HTML
                      //         return Column(children: children);
                      //     }
                      //   }
                      // },
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
