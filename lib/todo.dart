import 'package:flutter/material.dart';
import 'package:cool_alert/cool_alert.dart';
import './add_todo.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:tugasakhirb01/home/SideBar.dart';
import 'package:tugasakhirb01/global.dart' as globals;
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(MaterialApp(
    title: "Joeurnnal",
    theme: ThemeData(
      primarySwatch: Colors.deepPurple,
    ),
    home: Display_ToDo(),
  ));
}

class Display_ToDo extends StatefulWidget {
  @override
  _DisplayTodo createState() => _DisplayTodo();
}

class _DisplayTodo extends State<Display_ToDo> {
  List<dynamic> extractedData = [];
  var bool = true;
  Future<void> database() async { //buat nampilin datanya
    const url = 'http://joeurnnal.herokuapp.com/todo/app';
    try {
      final response = await http.get(Uri.parse(url));
      print(response.body);
      extractedData = jsonDecode(response.body);
      List<dynamic> temp = [];
      extractedData.forEach((val) {
        print(val);
        if(val['fields']['akun'] == globals.GlobalData.user){
          temp.add(val);
        }
      });
      extractedData = temp;
      setState((){});
    } catch (error) {
      print(error);
    }
  }

  @override

  Widget build(BuildContext context) {
    if(bool){
      database();
      bool = false;
    }
    return MaterialApp(
      title: 'Joeurnal',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
      ),
      home: Scaffold(
        drawer: SideBar(),
        appBar: AppBar(
          centerTitle: true,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                "좋은날",
                style:
                    GoogleFonts.poppins(fontSize: 20, color: Color(0xFF7862AE)),
              ),
              Text(
                " || To Do List",
                style: GoogleFonts.poppins(fontSize: 20),
              ),
            ],
          ),
          backgroundColor: Color(0xFF302C2C),
        ),
        body: Container(
          child: ListView(children: <Widget>[
              Image(
                image: AssetImage('assets/todo.png'),
              ),
              for (var i = 0; i < extractedData.length; i++)
                Card(
                elevation: 10.0,
                color : Color(0xFF7862AE),
                child: Container(
                  padding: EdgeInsets.all(10.0),
                  height: 90,
                  child : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        extractedData[i]['fields']['task'],
                        style: TextStyle(fontSize: 20.0,color: Colors.white,fontWeight: FontWeight.bold, fontFamily: 'Poppins'),
                      ),
                      SizedBox(height: 10.0),
                      Text(
                        extractedData[i]['fields']['deadline'],
                        style: TextStyle(fontSize: 15.0,color: Colors.white, fontFamily: 'Poppins'),
                      )
                    ],
                  ),
                ),
              ), //card

            Column(
              children: [
                SizedBox(height: 7.0),
                RaisedButton(onPressed: (){
                  Navigator.of(context)
                    .push(
                      MaterialPageRoute(
                        builder: (context) => BelajarForm()
                      )
                    );
                }, 
                elevation: 10.0,
                color: Color(0xFF7862AE),
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
                child: Text('Add New Task', style: TextStyle(color: Colors.white, fontFamily: 'Poppins'),),           
                ),
              ],
            ),
          ],),
 
        ),
      ),
    );
  }
}

