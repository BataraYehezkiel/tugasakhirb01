import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:provider/src/provider.dart';
import 'package:tugasakhirb01/add_todo.dart';
import 'package:tugasakhirb01/home/editprofile.dart';
import 'package:tugasakhirb01/utility/CookieRequest.dart';
import 'dart:convert';

class ProfileScreen extends StatefulWidget {
  ProfileScreen2 createState() => ProfileScreen2();
}

class ProfileScreen2 extends State<ProfileScreen> {
  var bool = true;
  List<dynamic> extractedData = [];
  Future<void> fetchData() async {
    const url = '10.0.2.2/profile/data';
    try {
      print("tes");
      final response = await http.get(Uri.parse(url));
      print(response.body);
      extractedData = jsonDecode(response.body);
      extractedData.forEach((val) {
        print(val);
      });
      setState(() {});
    } catch (error) {
      print(error);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (bool) {
      fetchData();
      bool = false;
    }
    final request = context.watch<CookieRequest>();
    String uname = request.username;
    return MaterialApp(
      title: 'Profile',
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xFF302C2C),
          centerTitle: true,
        ),
        body: ListView(
          children: <Widget>[
            Container(
              height: 250,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [Colors.purple.shade100, Colors.purple.shade50],
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      CircleAvatar(
                        backgroundColor: Colors.white70,
                        minRadius: 60.0,
                        child: CircleAvatar(
                          radius: 50.0,
                          backgroundImage: NetworkImage(
                              'https://avatars0.githubusercontent.com/u/28812093?s=460&u=06471c90e03cfd8ce2855d217d157c93060da490&v=4'),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
            Container(
              child: Column(
                children: <Widget>[
                  ListTile(
                    title: Text(
                      'Username',
                      style: TextStyle(
                        color: Colors.deepPurple,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    subtitle: Text(
                      '$uname',
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                  Divider(),
                  ListTile(
                    title: Text(
                      'Email',
                      style: TextStyle(
                        color: Colors.deepPurple,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    subtitle: Text(
                      'https://github.com/leopalmeiro',
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                  Divider(),
                  for (var item in extractedData)
                    ListTile(
                      title: Text(
                        'Phone',
                        style: TextStyle(
                          color: Colors.deepOrange,
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      subtitle: Text(
                        item['fields']['phone'],
                        style: TextStyle(fontSize: 18),
                      ),
                    ),
                  TextButton(
                    child: const Text('Edit'),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => BelajarFormP()));
                    },
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
