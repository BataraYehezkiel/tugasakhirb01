import 'package:flutter/material.dart';
import 'package:tugasakhirb01/todo.dart';


class TodoScreen extends StatelessWidget {
  const TodoScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Display_ToDo(),
    );
  }
}