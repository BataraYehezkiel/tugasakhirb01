import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/src/provider.dart';
import 'package:tugasakhirb01/auth/Screens/Landing/landing_screen.dart';
import 'package:tugasakhirb01/message.dart';
import 'package:tugasakhirb01/utility/CookieRequest.dart';
import 'package:tugasakhirb01/views/home.dart';
import 'home.dart';
import 'profile.dart';
import 'notes.dart';
import 'todo.dart';
import 'blogs.dart';
import 'message.dart';
import 'logout.dart';
import 'package:tugasakhirb01/contact.dart';
import 'package:tugasakhirb01/homepage.dart';


class SideBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final request = context.watch<CookieRequest>();
    String uname = request.username;
    return Drawer(
      child: ListView(
        // padding: EdgeInsets.zero,
        // margin: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            padding: EdgeInsets.zero,
            margin: EdgeInsets.zero,
            child: Container(
              padding: EdgeInsets.only(top: 20),
              child: Text('Joeurnnal \n좋은날',
                  style: GoogleFonts.poppins(color: Colors.white, fontSize: 35),
                  textAlign: TextAlign.center),
              // decoration: BoxDecoration(
              color: Color.fromRGBO(47, 47, 47, 1),
              // ),
            ),
          ),
          Container(
            color: Color.fromRGBO(111, 80, 161, 1),
            height: MediaQuery.of(context).size.height,
            child: Column(
              children: [
                Padding(padding: EdgeInsets.only(top: 15)),
                Text('Welcome! $uname',
                    style:
                        GoogleFonts.poppins(color: Colors.white, fontSize: 25),
                    textAlign: TextAlign.center),
                ListTile(
                  leading: Icon(Icons.home, color: Colors.white),
                  title: Text('Home',
                      style: GoogleFonts.poppins(
                          color: Colors.white, fontSize: 18)),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => HomePage(title:'joeurnnal'),
                      ),
                    );
                  },
                ),
                ListTile(
                  leading: Icon(Icons.people, color: Colors.white),
                  title: Text('Profile',
                      style: GoogleFonts.poppins(
                          color: Colors.white, fontSize: 18)),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ProfileScreen(),
                      ),
                    );
                  },
                ),
                ListTile(
                  leading: Icon(Icons.description, color: Colors.white),
                  title: Text('Notes',
                      style: GoogleFonts.poppins(
                          color: Colors.white, fontSize: 18)),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => NotesScreen(),
                      ),
                    );
                  },
                ),
                ListTile(
                  leading: Icon(Icons.share, color: Colors.white),
                  title: Text('To Do',
                      style: GoogleFonts.poppins(
                          color: Colors.white, fontSize: 18)),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => TodoScreen(),
                      ),
                    );
                  },
                ),
                ListTile(
                  leading: Icon(Icons.auto_stories, color: Colors.white),
                  title: Text('Public Blogs',
                      style: GoogleFonts.poppins(
                          color: Colors.white, fontSize: 18)),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Home(),
                      ),
                    );
                  },
                ),
                ListTile(
                  leading: Icon(Icons.message, color: Colors.white),
                  title: Text('Messages',
                      style: GoogleFonts.poppins(
                          color: Colors.white, fontSize: 18)),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => MyApp3(),
                      ),
                    );
                  },
                ),
                ListTile(
                  leading: Icon(Icons.logout, color: Colors.white),
                  title: Text('Logout',
                      style: GoogleFonts.poppins(
                          color: Colors.white, fontSize: 18)),
                  onTap: () async {
                    final response = await request.logoutAccount(
                      "https://joeurnnal.herokuapp.com/flutter_logout/"
                    );

                    if (response['status']) {
                      ScaffoldMessenger.of(context)
                          .showSnackBar(const SnackBar(
                        content: Text("Successfully Logged out!"),
                      ));
                      Navigator.pushReplacementNamed(context, LandingScreen.routeName);
                    } else {
                      ScaffoldMessenger.of(context)
                          .showSnackBar(const SnackBar(
                        content:
                            Text("An error occured, please try again."),
                      ));
                    }
                  },
                ),
              ],
            ),
          ),
        ],
      ), //lListView
    ); //drawer
  }
}
