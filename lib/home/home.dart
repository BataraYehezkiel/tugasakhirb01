import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:tugasakhirb01/global.dart' as globals;
class HomeScreen extends StatefulWidget{
HomeScreen2 createState() => HomeScreen2();
}
class HomeScreen2 extends State<HomeScreen> {
  var bool = true;
  List<dynamic> extractedData = [];
  List<dynamic> data = [];
  Future<void> fetchData() async {
  const url = 'http://joeurnnal.herokuapp.com/venska_home/data';
  try{
      print("tes");
    final response = await http.get(Uri.parse(url));
    print(response.body);
    extractedData = [];
    data = jsonDecode(response.body);
    data.forEach((val){
      if(globals.GlobalData.user == val['fields']['akun'])
      extractedData.add(val);
      print(val);
    });
    setState((){});
  }
  catch(error){
    print(error);
  }
}

  @override
  Widget build(BuildContext context) {
      if (bool){
          fetchData();
          bool = false;
      }
    return SizedBox(
      child: Column(
      children: [
          Padding(padding: EdgeInsets.only(top:20)),
          Text("Your schedule here!", style: GoogleFonts.poppins(color: Colors.black, fontSize: 45)),
          Padding(padding: EdgeInsets.only(left: 8)),
          for (var item in extractedData)
            Card(
                child: Column(
                    mainAxisSize: MainAxisSize.min,
            children: <Widget>[
                ListTile(
                leading: Icon(Icons.album),
                title: Text(item['fields']['task']),
                subtitle: Text(item['fields']['deadline']),
                ),
            ],
                ),
            ),
      ],
      ),
    );
  }
}