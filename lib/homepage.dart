import 'package:flutter/material.dart';
import 'package:tugasakhirb01/views/home.dart';
import './message.dart';
import 'home/SideBar.dart';
import 'package:tugasakhirb01/home/home.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key, required this.title}) : super(key: key);
  final String title;
  static const routeName = '/myhomepage_screen';

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideBar(),
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              "좋은날",
              style: TextStyle(
                  fontSize: 20,
                  color: Color(0xFF7862AE),
                  fontWeight: FontWeight.bold),
            ),
          ],
        ),
        backgroundColor: Color(0xFF302C2C),
      ),
      body: HomeScreen(),
    );
  }
}
