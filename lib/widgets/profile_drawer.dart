import 'package:flutter/material.dart';

//import '../screens/filters_screen.dart';

class MainDrawer extends StatelessWidget {
  Widget buildListTile(String title) {
    return ListTile(
        title: Text(
      title,
      style: TextStyle(
        fontFamily: 'RobotoCondensed',
        fontSize: 24,
        fontWeight: FontWeight.bold,
      ),
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            height: 120,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            alignment: Alignment.centerLeft,
            color: Theme.of(context).accentColor,
            child: Text(
              'Joeurnnal',
              style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 30,
                  color: Colors.black),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          buildListTile('Home'),
          buildListTile('Profile'),
          buildListTile('Notes'),
          buildListTile('To Do'),
          buildListTile('Public Blogs'),
          buildListTile('Message')
        ],
      ),
    );
  }
}
