class BlogsModel {
  String? model;
  int? pk;
  Fields? fields;

  BlogsModel({this.model, this.pk, this.fields});

  BlogsModel.fromJson(Map<String, dynamic> json) {
    model = json['model'];
    pk = json['pk'];
    fields =
        json['fields'] != null ? new Fields.fromJson(json['fields']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['model'] = this.model;
    data['pk'] = this.pk;
    if (this.fields != null) {
      data['fields'] = this.fields?.toJson();
    }
    return data;
  }
}

class Fields {
  String? title;
  String? author;
  String? content;

  Fields({this.title, this.author, this.content});

  Fields.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    author = json['author'];
    content = json['content'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    data['author'] = this.author;
    data['content'] = this.content;
    return data;
  }
}
