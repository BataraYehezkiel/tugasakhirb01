import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import './contact.dart';
import 'package:tugasakhirb01/home/SideBar.dart';
import 'package:tugasakhirb01/global.dart' as globals;
void main() {
  runApp(const MyApp2());
}

Future<void> fetchData() async {
  const url = 'http://joeurnnal.herokuapp.com/message/data/';
  try{
    final response = await http.get(Uri.parse(url));
    print(response.body);
    Map<String, dynamic> extractedData = jsonDecode(response.body);
    extractedData.forEach((key,val){
      print(val);
    });
  }
  catch(error){
    print(error);
  }
}
Future<http.Response> sendData(var _kepada,var _dari,var _isi) async{
  return http.post(Uri.parse('http://joeurnnal.herokuapp.com/message/get/'),
  headers: <String,String>{
    'Content-Type' : 'application/json; charset=UTF-8',
  },
  body: jsonEncode(<String,String>{
    'kepada': _kepada,
    'isi': _isi,
    'dari' : _dari
  }),
  );
}
class MyApp2 extends StatelessWidget {
  const MyApp2({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Message',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Message'),
    );
  }
}

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();
  var _kepada = "";
  var _dari = "";
  var _isi = "";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children:[
           TextButton(child: const Text('Send')
            ,onPressed:() {
              
                      Navigator.push(context,MaterialPageRoute(builder :(context) => MyApp2()));
              
              },),
              TextButton(child: const Text('Contact')
            ,onPressed:() {
              
                      Navigator.push(context,MaterialPageRoute(builder :(context) => MyApp3()));
              
              },),
          Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    initialValue: globals.GlobalData.user,
                    readOnly: true,
                    decoration: new InputDecoration(
                      hintText: "Contoh : nama",
                      labelText: "Dari",
                      icon: Icon(Icons.people),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Dari tidak boleh kosong';
                      }
                      print(value);
                      _dari = value;
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    
                    decoration: new InputDecoration(
                      hintText: "Contoh : nama",
                      labelText: "Kepada",
                      icon: Icon(Icons.people),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Kepada tidak boleh kosong';
                      }
                      print(value);
                      _kepada = value;
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      hintText: "Contoh : Isi",
                      labelText: "Isi",
                      icon: Icon(Icons.people),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Isi tidak boleh kosong';
                      }
                      print(value);
                      _isi = value;
                      return null;
                    },
                  ),
                ),
                TextButton(child: const Text('message')
            ,onPressed:() {Navigator.push(context,MaterialPageRoute(builder :(context) => MyApp3()));},),
                ElevatedButton(
                  child: Text(
                    "Submit",
                    style: TextStyle(color: Colors.white),
                  ),
                  
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      print(sendData(_kepada,_dari,_isi));
                    }
                  },
                ),
                // ElevatedButton(
                //   child: Text(
                //     "Submit",
                //     style: TextStyle(color: Colors.white),
                //   ),
                  
                //   onPressed: () => fetchData(),
                // ),
              ],
            ),
          ),
          ),
        ),
        ]),
      floatingActionButton: FloatingActionButton(onPressed: () => fetchData()),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }
  Widget buildListTile(String title, IconData icon, Function tapHandler) {
    return ListTile(
      leading: Icon(
        icon,
        size: 26,
      ),
      title: Text(
        title,
        style: TextStyle(
          fontFamily: 'RobotoCondensed',
          fontSize: 24,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: () => tapHandler
    );
  }
  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    final ButtonStyle flatButtonStyle = TextButton.styleFrom(
  primary: Colors.black87,
  minimumSize: Size(88, 36),
  padding: EdgeInsets.symmetric(horizontal: 16.0),
  shape: const RoundedRectangleBorder(
    borderRadius: BorderRadius.all(Radius.circular(2.0)),
  ),
);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              "좋은날",
              style: TextStyle(
                  fontSize: 20,
                  color: Color(0xFF7862AE),
                  fontWeight: FontWeight.bold),
            ),
          ],
        ),
        backgroundColor: Color(0xFF302C2C),
      ),
      body: BelajarForm(),
      
      drawer:SideBar(), 
      
    );
  }
}
