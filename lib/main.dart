import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tugasakhirb01/auth/Screens/Landing/landing_screen.dart';
import 'package:tugasakhirb01/utility/CookieRequest.dart';
import 'package:tugasakhirb01/homepage.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Provider(
        create: (_) {
          CookieRequest request = CookieRequest();
          return request;
        },
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Joeurnnal',
          theme: ThemeData(
            primarySwatch: Colors.purple,
            scaffoldBackgroundColor: Colors.white,
          ),
          home: LandingScreen(),
          routes: {
              LandingScreen.routeName: (ctx) => LandingScreen(),
              HomePage.routeName: (ctx) => HomePage(title: 'Joeurnnal'),
          },
        ),
    );    
  }
}

