import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:http/http.dart' as http;
import 'package:tugasakhirb01/home/SideBar.dart';
import 'dart:convert';
import 'package:tugasakhirb01/global.dart' as globals;

import 'note_form.dart';
// import 'package:lab_7/screen/note_form.dart';

class CardNote2 extends StatefulWidget {
  @override
  CardNote createState() => CardNote();
}

class CardNote extends State<CardNote2> {
  List<dynamic> extractedData = [];
  List<dynamic> temp = [];

  var bool = true;

  Future<void> database() async {
    // const url = 'http://joeurnnal.herokuapp.com/message/data/';
    const url = 'http://joeurnnal.herokuapp.com/take-a-note/add-flutter';
    // try {Today
    final response = await http.get(Uri.parse(url));
    // print(response.body);
    extractedData = jsonDecode(response.body);
    temp = [];
    extractedData.forEach((val) {
      if (val['fields']['akun'] == globals.GlobalData.user) {
        temp.add(val);
      }
    });

    extractedData = temp;

    //     extractedData.forEach((val){
    //   if(((val['fields']['kepada'] == globals.GlobalData.user) && (val['fields']['dari'] == widget.dari )) || ((val['fields']['dari'] == globals.GlobalData.user) && (val['fields']['kepada'] == widget.dari )) )
    //   {
    //     data.add(val);
    //     daftarKey.add(GlobalKey<FormState>());
    //     print(val);
    //   }
    // });

    setState(() {});
    // } catch (error) {
    //   print(error);
    // }
  }

  @override
  Widget build(BuildContext context) {
    // database();
    // print(extractedData.length);
    // Future.delayed(Duration(seconds: 2), () {});
    if (bool) {
      database();
      bool = false;
    }

    return MaterialApp(
      title: 'Joeurnnal 좋은날',
      home: Scaffold(
      drawer: SideBar(),
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children:<Widget>[
            Text("좋은날", style:GoogleFonts.poppins(fontSize: 20, color: Color(0xFF7862AE)),),
            Text(" || Note",style: GoogleFonts.poppins(fontSize: 20),),
          ],
        ),
        backgroundColor: Color(0xFF302C2C),
        // Text("Joeurnnal 좋은날",
        //       style: GoogleFonts.poppins(),
        // // ),
        // backgroundColor: Colors.grey[900],
      ),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/backgpag.png'),
              fit: BoxFit.cover,
            ),
          ),
          padding: EdgeInsets.all(10.0),
          child: ListView(
            children: <Widget>[
              SizedBox(height: 10),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 40.0,
                    child: RaisedButton(
                      // padding: EdgeInsets.all(10.0),

                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => NoteForm()));
                      },
                      elevation: 10.0,
                      color: Color.fromRGBO(141, 129, 204, 1),
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.0))),
                      child: Text(
                        '+   New Note',
                        style: GoogleFonts.poppins(
                          color: Colors.white,
                          fontSize: 18,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                height: 20,
              ),
              for (var item in extractedData)
                // Padding(
                //   padding: EdgeInsets.only(
                //     top: 10.0,
                //   ),
                //   child:
                Container(
                  height: 500,
                  child: Card(
                    elevation: 10.0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                    color: Color.fromRGBO(141, 129, 204, 1),
                    child: Container(
                      padding: EdgeInsets.all(20.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            child: Row(
                              children: [
                                // ButtonBar(
                                //   children: [
                                //     IconButton(
                                //         onPressed: () {
                                //           // print(item['pk']);
                                //           sendData(item['pk'].toString());
                                //           // bool = true;
                                //           // database();
                                //           setState(() {
                                //             extractedData.remove(item);
                                //           });
                                //         },
                                //         icon: Icon(Icons.delete),
                                //         color: Colors.white)
                                //   ],
                                // ),
                                Text(
                                  item['fields']['judul'],
                                  // "Judul",
                                  style: GoogleFonts.poppins(
                                    fontSize: 25.0,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                  ),
                                  // TextStyle(
                                  //     fontSize: 25.0,
                                  //     fontWeight: FontWeight.w700,
                                  //     color: Colors.white,
                                  //     fontFamily: 'Poppins',),
                                ),
                                Spacer(),
                                Align(
                                  alignment: Alignment.topRight,
                                  child: ButtonBar(
                                    children: [
                                      IconButton(
                                          onPressed: () {
                                            // print(item['pk']);
                                            sendData(item['pk'].toString());
                                            // bool = true;
                                            // database();
                                            setState(() {
                                              extractedData.remove(item);
                                            });
                                          },
                                          icon: Icon(Icons.delete),
                                          color: Colors.white)
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                              top: 10,
                            ),
                            child: Expanded(
                                child: Text(
                              item['fields']['isi'],
                              // "Isi",
                              textAlign: TextAlign.justify,
                              style: GoogleFonts.poppins(
                                fontSize: 18.0,
                                color: Colors.white,
                              ),
                              // TextStyle(
                              //   fontWeight: FontWeight.w500,
                              //   fontSize: 20.0,
                              //   color: Colors.white,
                              //   fontFamily: 'Poppins',
                              // ),
                            )),
                          )
                        ],
                      ),
                    ),
                  ), //card,
                )
            ],
          ),
        ),
      ),
    );
  }
}

Future<http.Response> sendData(var _pk) async {
  return http.post(
    Uri.parse('http://joeurnnal.herokuapp.com/take-a-note/del-data'),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, String>{
      'pk': _pk,
    }),
  );
}
