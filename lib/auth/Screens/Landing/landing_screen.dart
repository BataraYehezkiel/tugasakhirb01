import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:tugasakhirb01/auth/Screens/Login/login_screen.dart' as login;
import 'package:tugasakhirb01/auth/Screens/SignUp/signup_screen.dart' as signup;


class LandingScreen extends StatefulWidget {
  static const routeName = '/landing_screen';

  @override
  _LandingScreenState createState() => _LandingScreenState();
}

class _LandingScreenState extends State<LandingScreen> with SingleTickerProviderStateMixin {
  late TabController controller;

  @override
  void initState(){
    controller = new TabController(vsync: this, length: 2);
    super.initState();
  }

  @override
  void dispose(){
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Color(0xffA78BFA),
         title: new Text("Joeurnnal 좋은날", style: GoogleFonts.poppins(),),
           bottom: new TabBar(
             controller: controller,
             tabs: <Widget>[
              new Tab(icon: new Icon(Icons.login), child: Text("Login", style: GoogleFonts.poppins())),
              new Tab(icon: new Icon(Icons.app_registration), child: Text("Signup", style: GoogleFonts.poppins())),
             ],
          ),
      ),
      body: new TabBarView(
        controller: controller,
        children: <Widget>[
          new login.LoginScreen(),
          new signup.SignupScreen()
        ],
      ),
    );
  }
}