import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:tugasakhirb01/auth/Components/background.dart';
import 'package:tugasakhirb01/auth/Components/rounded_button.dart';
import 'package:tugasakhirb01/auth/Components/rounded_input_field.dart';
import 'package:tugasakhirb01/auth/Components/rounded_password_field.dart';
import 'package:tugasakhirb01/auth/Components/title_form.dart';
import 'package:tugasakhirb01/auth/Screens/Landing/landing_screen.dart';
import 'package:tugasakhirb01/homepage.dart';
import 'package:tugasakhirb01/utility/CookieRequest.dart';
import 'package:tugasakhirb01/global.dart' as globals;

class Body extends StatelessWidget {
  final _loginFormKey = GlobalKey<FormState>();
  String username = "";
  String password = "";

  Body({
    Key? key,
  }) : super(key: key);


  @override
  Widget build(BuildContext context) {
    final request = context.watch<CookieRequest>();
    Size size = MediaQuery.of(context).size;
    RoundedInputField temp1 = RoundedInputField(text: 'Username', inputType: TextInputType.name, input: username);
    RoundedPasswordField temp2 = RoundedPasswordField(text: 'Password', input: password);

    return AnnotatedRegion<SystemUiOverlayStyle> (
      value: SystemUiOverlayStyle.light,
      child: GestureDetector(
        child: Stack(
          children: <Widget>[
            Form(
              key: _loginFormKey,
              child: Background(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    // Text('Login Form', style: GoogleFonts.poppins()),
                    TitleForm(text: 'Login Form'),
                    SizedBox(height: 50,),
                    temp1,
                    SizedBox(height: 20,),
                    temp2,
                    RoundedButton(
                      text: 'LOGIN',
                      press: () async {
                        if (_loginFormKey.currentState!.validate()) {
                          final response = await request.login(
                              "https://joeurnnal.herokuapp.com/flutter_login/", {
                              'username': temp1.input,
                              'password': temp2.input,
                          });
                          
                          if (response['status']) {
                            ScaffoldMessenger.of(context)
                                .showSnackBar(const SnackBar(
                              content: Text("Akun telah berhasil masuk!"),
                            ));
                            globals.GlobalData.user = temp1.input; 
                            Navigator.pushReplacementNamed(context, HomePage.routeName);
                          } else {
                            ScaffoldMessenger.of(context)
                                .showSnackBar(const SnackBar(
                              content:
                                  Text("An error occured, please try again."),
                            ));
                          }
                        } 
                        
                        else {
                          ScaffoldMessenger.of(context)
                                .showSnackBar(const SnackBar(
                              content: Text("Login tidak valid!"),
                            ));
                          Navigator.pushReplacementNamed(context, LandingScreen.routeName);
                        }
                      },
                    ),
                  ]
                ),
              ),
            ),
          ],
        )
      )
    );
  }
}