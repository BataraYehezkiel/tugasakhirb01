import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:tugasakhirb01/auth/Components/background.dart';
import 'package:tugasakhirb01/auth/Components/rounded_button.dart';
import 'package:tugasakhirb01/auth/Components/rounded_input_field.dart';
import 'package:tugasakhirb01/auth/Components/rounded_password_field.dart';
import 'package:tugasakhirb01/auth/Components/title_form.dart';
import 'package:tugasakhirb01/auth/Screens/Landing/landing_screen.dart';


class Body extends StatelessWidget {
  final _signupFormKey = GlobalKey<FormState>();
  String username = "";
  String email = "";
  String password = "";

  Body({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    RoundedInputField temp1 = RoundedInputField(text: 'Username', inputType: TextInputType.name, input: username);
    RoundedInputField temp2 = RoundedInputField(text: 'Email', inputType: TextInputType.emailAddress, input: email);
    RoundedPasswordField temp3 = RoundedPasswordField(text: 'Password', input: password);

    return AnnotatedRegion<SystemUiOverlayStyle> (
      value: SystemUiOverlayStyle.light,
      child: GestureDetector(
        child: Stack(
          children: <Widget>[
            Form(
              key: _signupFormKey,
              child: Background(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    TitleForm(text: 'SignUp Form'),
                    SizedBox(height: 50,),
                    temp1,
                    SizedBox(height: 20,),
                    temp2,
                    SizedBox(height: 20,),
                    temp3,
                    RoundedButton(
                      text: 'SIGNUP',
                      press: () async {
                        if (_signupFormKey.currentState!.validate()) {
                          final response = await http.post(Uri.parse("https://joeurnnal.herokuapp.com/flutter_register/"),
                            headers: <String, String>{
                              'Content-Type': 'application/json;charset=UTF-8',
                            },
                            body: jsonEncode(<String, String>{
                              'username': temp1.input,
                              'email': temp2.input,
                              'password': temp3.input,
                            })
                          );

                          var responseData = json.decode(response.body);
                          if (responseData['status'] == 'success') {
                            ScaffoldMessenger.of(context).showSnackBar(
                              const SnackBar(
                                content: Text(
                                    "Account has been successfully registered!"
                                ),
                              )
                            );
                            Navigator.pushReplacementNamed(context, LandingScreen.routeName);
                          }
                        } 
                        
                        else {
                          ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                              content: Text(
                                  "An error occured, please try again."
                              ),
                            )
                          );
                          Navigator.pushReplacementNamed(context, LandingScreen.routeName);
                        }
                      },
                    ),
                  ]
                ),
              ),
            ),  
          ],
        )
      )
    );
  }
}